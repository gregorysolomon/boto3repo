

'''
You need to have AWS credentials in ~/.aws/credentials
[default]
aws_access_key_id=KEY_ID
aws_secret_access_key=ACCESS_KEY
'''

from flask import Flask, request, flash
import boto3

app = Flask(__name__)

@app.route('/')
def index():
    return '''<form method=POST enctype=multipart/form-data action="upload">
    <input type=file name=myfile>
    <input type=submit>
    </form>'''

@app.route('/upload', methods=['GET','POST'])
def upload():
    s3=boto3.resource('s3')
    file=request.files['myfile']
    s3.Bucket('YOUR BUCKET NAME HERE').put_object(Key=file.filename, Body=request.files['myfile'])
    return '<h1>File Uploaded</h1>'

@app.route('/create')
def create():
    s3=boto3.client('s3')
    s3.create_bucket(Bucket='YOUR BUCKET NAME HERE')
    return '<h1>Bucket Created</h1>'

@app.route('/download')
def download():
    return '<h1>Downloaded</h1>'

if __name__ == '__main__':
    app.secret_key='test'
    app.run(debug=True)
